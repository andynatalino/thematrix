    <?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/


Route::get('/', function () {
    return view('welcome');
});

Auth::routes();

Route::get('/', 'Customer\HomeController@index')->name('index');


Route::get('/home', 'Customer\HomeController@shop')->name('home');
Route::get('shop', 'Customer\HomeController@shop')->name('customer.shop');
Route::get('shop/search', 'Customer\HomeController@shopSearch');
Route::get('shop/{id}/show', 'Customer\HomeController@shopShow')->name('customer.shopShow');
Route::get('shop/{id}/search', 'Customer\HomeController@shopSearchShow');
Route::get('shop/{id}/product', 'Customer\HomeController@shopProduct')->name('customer.shopProduct');
Route::post('shop/product', 'Customer\HomeController@shopBuy')->name('customer.shopBuy.submit');
Route::get('transaction', 'Customer\HomeController@transaction')->name('customer.transaction');
Route::get('settings', 'Customer\HomeController@settings')->name('customer.settings');
Route::post('settings', 'Customer\HomeController@settingsSave')->name('customer.settingsSave');
Route::get('settings/change-password', 'Customer\HomeController@changepassword')->name('customer.changepassword');
Route::post('settings/change-password', 'Customer\HomeController@changepasswordSave')->name('customer.changepasswordSave.submit');

Route::group(['prefix' => 'supplier', 'namespace' => 'Supplier'], function () {    
    Route::get('/', 'Auth\LoginController@showLoginForm')->name('supplier.login');
    Route::post('login', 'Auth\LoginController@login')->name('supplier.login.submit');
    Route::post('logout', 'Auth\LoginController@logout')->name('supplier.logout');
    Route::get('register', 'Auth\RegisterController@showRegistrationForm')->name('supplier.register');
    Route::post('register', 'Auth\RegisterController@register')->name('supplier.register.submit');

    Route::get('home', 'HomeController@index')->name('supplier.home');

    Route::get('product', 'HomeController@product')->name('supplier.product');
    Route::get('product/add', 'HomeController@productAdd')->name('supplier.productAdd');
    Route::post('product/add', 'HomeController@productSave')->name('supplier.productSave.submit');
    Route::get('product/{id}/edit', 'HomeController@productEdit');
    Route::post('product', 'HomeController@productUpdate')->name('supplier.productUpdate.submit');
    Route::delete('product/{id}', 'HomeController@productDelete');

    Route::get('transaction', 'HomeController@transaction')->name('supplier.transaction');
    Route::get('transaction/{id}/confirm', 'HomeController@transactionConfirm');
    Route::post('transaction', 'HomeController@transactionConfirmSave')->name('supplier.transactionConfirmSave.submit');

    Route::get('transaction-success', 'HomeController@transactionsuccess')->name('supplier.transactionsuccess');
    Route::get('transaction-success/{id}/edit', 'HomeController@transactionsuccessEdit');
    Route::post('transaction-success', 'HomeController@transactionsuccessUpdate')->name('supplier.transactionsuccessUpdate.submit');
    Route::delete('transactionsuccess/{id}', 'HomeController@transactionsuccessDelete');

    Route::get('settings', 'HomeController@settings')->name('supplier.settings');
    Route::post('settings', 'HomeController@settingsSave')->name('supplier.settingsSave.submit');
    Route::get('settings/change-password', 'HomeController@settingsChangePassword')->name('supplier.settingsChangePassword');
    Route::post('settings/change-password', 'HomeController@settingsChangePasswordSave')->name('supplier.settingsChangePasswordSave.submit');
});

Route::group(['prefix' => 'admin', 'namespace' => 'Admin'], function () {
    Route::get('/', 'Auth\LoginController@showLoginForm')->name('admin.login');
    Route::post('login', 'Auth\LoginController@login')->name('admin.login.submit');
    Route::post('logout', 'Auth\LoginController@logout')->name('admin.logout');
    Route::get('home', 'HomeController@index')->name('admin.home');

    Route::get('supplier/{id}/confirm', 'HomeController@supplierConfirm');
    Route::post('supplier', 'HomeController@supplierConfirmSave')->name('supplier.supplierConfirmSave.submit');
    Route::delete('supplier/{id}', 'HomeController@supplierDelete');

    Route::get('store-category', 'HomeController@storeCategory')->name('admin.storeCategory');
    Route::get('store-category/add', 'HomeController@storeCategoryAdd')->name('admin.storeCategoryAdd');
    Route::post('store-category/add', 'HomeController@storeCategorySave')->name('admin.storeCategorySave.submit');
    Route::get('store-category/{id}/edit', 'HomeController@storeCategoryEdit')->name('admin.storeCategoryEdit');
    Route::post('store-category', 'HomeController@storeCategoryUpdate')->name('admin.storeCategoryUpdate.submit');
    Route::delete('store-category/{id}', 'HomeController@storeCategoryDelete');

    Route::get('product-category', 'HomeController@productCategory')->name('admin.productCategory');
    Route::get('product-category/add', 'HomeController@productCategoryAdd')->name('admin.productCategoryAdd');
    Route::post('product-category/add', 'HomeController@productCategorySave')->name('admin.productCategorySave.submit');
    Route::get('product-category/{id}/edit', 'HomeController@productCategoryEdit')->name('admin.productCategoryEdit');
    Route::post('product-category', 'HomeController@productCategoryUpdate')->name('admin.productCategoryUpdate.submit');
    Route::delete('product-category/{id}', 'HomeController@productCategoryDelete');
});

