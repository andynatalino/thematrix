@extends('customer.layouts.app')

@section('title','Product - Supplier')

@section('content')
<p>
	Hi {{ Auth::user()->name }}, welcome to {{ $supplier->nama }}<br>
	{{ $supplier->slogan }}<br>
	Address : {{ $supplier->alamat }}<br>
</p> 

<form method="get" action="{{ url('shop/'.$supplier->id.'/search') }}">
	<div class="row">
		<div class="form-group col-md-6">
			<input type="text" class="form-control submit_on_enter" name="q" placeholder="Search Products">
		</div>
	</div>
</form>

<table class="table table-striped">
	<thead>
		<tr>
			<th>Product Name</th>
			<th>Category</th>
			<th>Price</th>
			<th>Stok</th>
			<th>Opsi</th>
		</tr>
	</thead>
	@foreach($product as $key)
	<tbody>      
		<td>{{ $key->nama }}</td>
		<td>{{ $key->jenis }}</td>
		<td>Rp. {{ $key->harga }},-</td>
		<td>{{ $key->stok }}</td>
		<td>
			<a href="{{ url('shop/'.$key->id.'/product')}}"><button type="button" class="btn btn-warning btn-sm">Buy</button></a>     
		</td>
	</tr>  
	@endforeach
</tbody>
</table>


@endsection