@extends('customer.layouts.app')

@section('title','Product - Supplier')

@section('content')

<h2>Shop</h2> 
<form method="get" action="{{ url('shop/search') }}">
	<div class="row">
		<div class="form-group col-md-6">
			<input type="text" class="form-control submit_on_enter" name="q" placeholder="Search Shop">
		</div>
	<div class="form-group col-md-6">
		<select class="form-control" id="store" name="q" onchange="this.form.submit()">
			<option disabled selected="selected">--Search by Category--</option>
			@foreach($store as $key)
			<option value="{{ $key->id }}">{{ $key->name }}</option>      
			@endforeach
		</select>
	</div>
</div>
</form>
<table class="table table-striped">
	<thead>
		<tr>
			<th>Store Name</th>
			<th>Address</th>
			<th>Category Store</th>
			<th>Opsi</th>
		</tr>
	</thead>
	@foreach($supplier as $key)
	<tbody>      
		<td>{{ $key->nama }}</td>
		<td>{{ $key->alamat }}</td>
		<td>{{ $key->jenistoko }}</td>
		<td>
			<a href="{{ url('shop/'.$key->id.'/show')}}"><button type="button" class="btn btn-warning btn-sm">Show Store</button></a>     
		</td>
	</tr>  
	@endforeach
</tbody>
</table>
{{ $supplier->links() }}

<script src="https://ajax.googleapis.com/ajax/libs/jquery/2.1.1/jquery.min.js"></script>

<script type="text/javascript">
	$(document).ready(function() {
		$('.submit_on_enter').keydown(function(event) {
    // enter has keyCode = 13, change it if you want to use another button
    if (event.keyCode == 13) {
    	this.form.submit();
    	return false;
    }
});

	});
</script>
@endsection