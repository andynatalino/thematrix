@extends('customer.layouts.app')

@section('title','Edit Product - Supplier')

@section('content')
<h2>Settings</h2>
<form method="post" action="{{ url('settings') }}">
  <div class="form-group">
    <label for="username">Username</label>
    <input type="text" class="form-control" id="username" name="username"  readonly value="{{ $user->username }}">
  </div>
  <div class="form-group">
    <label for="nama">Nama</label>
    <input type="text" class="form-control" id="nama" name="nama" placeholder="Enter Nama" value="{{ $user->name }}">
  </div>
  <div class="form-group">
    <label for="email">E-Mail</label>
    <input type="text" class="form-control" id="email" name="email" placeholder="Enter E-Mail" required value="{{ $user->email }}">
  </div>
  <div class="form-group">
    <a href="{{ url('/settings/change-password')}}"><button type="button" class="btn">Change Password</button></a>
  </div>
  {{ csrf_field()}}
  <button type="submit" class="btn btn-primary">Save</button>
  <input type="hidden" name="id" value="{{ $user->id }}">
  <a href="{{ url('supplier/settings')}}" class="btn btn-button">Cancel</a>
</form> 
@endsection