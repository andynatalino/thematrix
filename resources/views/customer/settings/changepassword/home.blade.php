@extends('customer.layouts.app')

@section('title','Add Product - Supplier')

@section('content')

<h2>Change Password</h2>
<form method="post" action="{{route('customer.changepasswordSave.submit')}}">
  <div class="form-group">
    <label for="oldpw">Old Password</label>
    <input type="password" class="form-control" id="oldpw" name="oldpw" placeholder="Enter Old Password" required>
  </div>
  <div class="form-group">
    <label for="newpw">New Password</label>
    <input type="password" class="form-control" id="newpw" name="newpw" placeholder="Enter New Password" required>
  </div>
  <div class="form-group">
    <label for="confnewpw">Repeat Password</label>
    <input type="password" class="form-control" id="confnewpw" name="confnewpw" placeholder="Enter Repeat password" required>
  </div>
  {{ csrf_field()}}
  <button type="submit" class="btn btn-primary">Save</button>
  <a href="{{ url('settings')}}" class="btn btn-button">Cancel</a>
</form> 

@if(session('gagal'))
{{ session('gagal')}}      
@endif


@endsection