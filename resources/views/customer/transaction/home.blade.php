@extends('customer.layouts.app')

@section('title','Transaction')

@section('content')
<h2>My Transaction</h2>  
<table class="table table-striped">
  <thead>
    <tr>
      <th>Transaction Code</th>
      <th>Customer</th>
      <th>Supplier</th>
      <th>Product</th>
      <th>Amount</th>
      <th>Information</th>
      <th>Date</th>
      <th>Status</th>
    </tr>
  </thead>
  <tbody>      
   @foreach($transactions as $key)
   <td>{{ $key->id }}</td>
   <td>{{ $key->customer->name }}</td>   
   <td>{{ $key->supplier->nama }}</td>
   <td>{{ $key->product->nama }}</td>
   <td>{{ $key->jumlah }}</td>
   <td>{{ substr($key->keterangan, 0, 10) }}</td>
   <td>{{ $key->tanggal }} </td>
   <td> @if($key->status == 1) <span class="badge badge-warning">Waiting</span> @elseif($key->status == 2) <span class="badge badge-success">Success</span> @endif </td>
 </tr>  
 @endforeach
</tbody>
</table>
{{ $transactions->links() }}
@endsection