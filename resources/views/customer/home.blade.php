@extends('customer.layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card card-default">
                <div class="card-header">Halo {{ Auth::user()->name }}</div>

                <div class="card-body">
                    @if (session('status'))
                    <div class="alert alert-success">
                        {{ session('status') }}
                    </div>
                    @endif
                    Simpan kode username anda, Kode Username digunakan ketika login<br>
                    Username Anda : <b>{{ Auth::user()->username }}</b>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
