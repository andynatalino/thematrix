@extends('customer.layouts.app')

@section('title','Product - Supplier')

@section('content')
<?php 
$con = App\Transaksi::where(['product_id' => $product->id, 'status' => 1])->get()->count();  
$kuota = $product->stok;
?>
<h1>Product Name : {{ $product->nama }}</h1><br>
<h2>Amount : Rp. {{ $product->harga }},-</h2><br>
<h2>Category : {{ $product->jenis }}</h2><br>
<h2>Stok : {{ $product->stok }}</h2><br>

<form method="post" action="{{ url('shop/product') }}">
	{{csrf_field()}}
	<input type="hidden" name="supplier_id" value="{{ $product->supplier_id }}">
	<input type="hidden" name="product_id" value="{{ $product->id }}">	
	<input type="hidden" name="information" value="{{ $product->keterangan }}">	
	<div class="form-group">
		<label for="amount">Amount</label>
		<input type="number" class="form-control" id="amount" name="amount" placeholder="Enter Amount" required>
	</div>
	<div class="form-group">
		<label for="information">Keterangan</label>
		<input type="text" class="form-control" id="information" name="information" placeholder="Enter Information" required>
	</div>	
	@if(!$con < $kuota)
	<button type="submit" class="btn btn-success btn-lg">Buy Now</button>

	@else
	<h2>Sorry, Product Sold Out</h2>
	<a href="{{ url('/shop/'.$product->supplier_id.'/show') }}"><button type="button" class="btn btn-success btn-lg">
	Find other products</button></a>
	@endif
	
</form>
@endsection