@extends('supplier.layouts.app')

@section('title','Add Product - Supplier')

@section('content')


@if(auth()->guard('supplier')->user()->approved == 1) 
Account supplier masih belum dapat digunakan, untuk mengaktifkannya 
Anda harus membayar sejumlah uang yang ditentukan oleh The Matrix Mall.

Hubungi Call Center kami di 911

<h1>Kode Username Anda</h1><br>
<h2>{{ auth()->guard('supplier')->user()->username }}</h2>

masukan username dan password anda ketika login

@elseif(auth()->guard('supplier')->user()->approved == 2) 

<h2>Change Password</h2>
<form method="post" action="{{route('supplier.settingsChangePasswordSave.submit')}}">
  <div class="form-group">
    <label for="oldpw">Old Password</label>
    <input type="password" class="form-control" id="oldpw" name="oldpw" placeholder="Enter Old Password" required>
  </div>
  <div class="form-group">
    <label for="newpw">New Password</label>
    <input type="password" class="form-control" id="newpw" name="newpw" placeholder="Enter New Password" required>
  </div>
  <div class="form-group">
    <label for="confnewpw">Repeat Password</label>
    <input type="password" class="form-control" id="confnewpw" name="confnewpw" placeholder="Enter Repeat password" required>
  </div>
  {{ csrf_field()}}
  <button type="submit" class="btn btn-primary">Save</button>
  <a href="{{ url('supplier/settings')}}" class="btn btn-button">Cancel</a>
</form> 

@if(session('gagal'))
{{ session('gagal')}}      
@endif

@endif

@endsection