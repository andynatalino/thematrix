@extends('supplier.layouts.app')

@section('title','Edit Product - Supplier')

@section('content')


@if(auth()->guard('supplier')->user()->approved == 1) 
Account supplier masih belum dapat digunakan, untuk mengaktifkannya 
Anda harus membayar sejumlah uang yang ditentukan oleh The Matrix Mall.

Hubungi Call Center kami di 911

<h1>Kode Username Anda</h1><br>
<h2>{{ auth()->guard('supplier')->user()->username }}</h2>

masukan username dan password anda ketika login

@elseif(auth()->guard('supplier')->user()->approved == 2) 

<h2>Settings</h2>

<form method="post" action="{{ url('supplier/settings') }}">
  <div class="form-group">
    <label for="username">Username</label>
    <input type="text" class="form-control" id="username" name="username" required disabled value="{{ $supplier->username }}">
  </div>
  <div class="form-group">
    <label for="storename">Store Name</label>
    <input type="text" class="form-control" id="storename" name="storename" placeholder="Enter Store Name" required value="{{ $supplier->nama }}">
  </div>
  <div class="form-group">
    <label for="address">Address</label>
    <textarea class="form-control" id="address" name="address" placeholder="Enter Address" required>{{ $supplier->alamat }}</textarea>
  </div>
  <div class="form-group">
    <label for="category">Store Category</label>
    <input type="text" class="form-control" id="category" name="category" placeholder="Enter Store Category" disabled="" value="{{ $supplier->storecategory->name }}">
  </div>
  <div class="form-group">
    <label for="email">E-Mail</label>
    <input type="text" class="form-control" id="email" name="email" placeholder="Enter Email" required value="{{ $supplier->email }}">
  </div>
  <div class="form-group">
    <label for="slogan">Slogan</label>
    <input type="text" class="form-control" id="slogan" name="slogan" placeholder="Enter Slogan" required value="{{ $supplier->slogan }}">
  </div>
  <div class="form-group">
    <label for="approved">Store Status :</label>
    @if($supplier->approved == 1) <span class="badge badge-danger">Not Active</span> @elseif($supplier->approved == 2) <span class="badge badge-success">Active</span> @endif
  </div>
  <div class="form-group">
    <a href="{{ url('supplier/settings/change-password')}}"><button type="button" class="btn">Change Password</button></a>
  </div>
  {{ csrf_field()}}
  <button type="submit" class="btn btn-primary">Save</button>
  <input type="hidden" name="id" value="{{ $supplier->id }}">  
</form> 
@endif

@endsection