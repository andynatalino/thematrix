@extends('supplier.layouts.app')

@section('title','Transaction - Supplier')

@section('content')


@if(auth()->guard('supplier')->user()->approved == 1) 
Account supplier masih belum dapat digunakan, untuk mengaktifkannya 
Anda harus membayar sejumlah uang yang ditentukan oleh The Matrix Mall.

Hubungi Call Center kami di 911

<h1>Kode Username Anda</h1><br>
<h2>{{ auth()->guard('supplier')->user()->username }}</h2>

masukan username dan password anda ketika login

@elseif(auth()->guard('supplier')->user()->approved == 2) 

<h2>Transaction Pending</h2>  
<table class="table table-striped">
  <thead>
    <tr>
      <th>Transaction Code</th>
      <th>Customer</th>
      <th>Supplier</th>
      <th>Product</th>
      <th>Amount</th>
      <th>Information</th>
      <th>Date</th>
      <th>Status</th>
      <th>Opsi</th>
    </tr>
  </thead>
  <tbody>      
   @foreach($transactions as $key)
   <td>{{ $key->id }}</td>
   <td>{{ $key->customer->name }}</td>   
   <td>{{ $key->supplier->nama }}</td>
   <td>{{ $key->product->nama }}</td>
   <td>{{ $key->jumlah }}</td>
   <td>{{ substr($key->keterangan, 0, 10) }}</td>
   <td>{{ $key->tanggal }} </td>
   <td> @if($key->status == 1) <span class="badge badge-warning">Waiting</span> @elseif($key->status == 2) <span class="badge badge-success">Success</span> @endif </td>
   <td>
    <a href="{{ url('supplier/transaction/'.$key->id.'/confirm')}}"><button type="button" class="btn btn-success btn-sm">Confirm</button></a>
   </td>
 </tr>  
 @endforeach
</tbody>
</table>
{{ $transactions->links() }}
@endif

@endsection