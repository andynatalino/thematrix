@extends('supplier.layouts.app')

@section('title','Transaction Confirm - Supplier')

@section('content')


@if(auth()->guard('supplier')->user()->approved == 1) 
Account supplier masih belum dapat digunakan, untuk mengaktifkannya 
Anda harus membayar sejumlah uang yang ditentukan oleh The Matrix Mall.

Hubungi Call Center kami di 911

<h1>Kode Username Anda</h1><br>
<h2>{{ auth()->guard('supplier')->user()->username }}</h2>

masukan username dan password anda ketika login

@elseif(auth()->guard('supplier')->user()->approved == 2) 

<div class="jumbotron">
 <h1 class="display-9">Transaction Confirm</h1>
 <div class="container">

  <form method="post" action="{{ url('supplier/transaction') }}">
    <div class="form-group">
      <label>Transaction Code :</label>
      {{ $transactions->id }}
      <input type="hidden" name="id" value="{{ $transactions->id }}">
    </div>
    <div class="form-group">
      <label>Customer :</label>
      {{ $transactions->customer->name }}
      <input type="hidden" name="customer" value="{{ $transactions->customer->id }}">
    </div>
    <div class="form-group">
      <label>Supplier :</label>
      {{ $transactions->supplier->nama }}
      <input type="hidden" name="supplier" value="{{ $transactions->supplier->id }}">
    </div>
    <div class="form-group">
      <label>Product :</label>
      {{ $transactions->product->nama }}
      <input type="hidden" name="product" value="{{ $transactions->product->id }}">
    </div>
    <div class="form-group">
      <label>Amount :</label>
      {{ $transactions->jumlah }}
      <input type="hidden" name="amount" value="{{ $transactions->jumlah }}">
    </div>
    <div class="form-group">
      <label>Information :</label>
      {{ $transactions->keterangan }}
      <input type="hidden" name="information" value="{{ $transactions->keterangan }}">
    </div>
    <div class="form-group">
      <label>Date of Purchase :</label>
      {{ $transactions->tanggal }}
      <input type="hidden" name="date" value="{{ $transactions->tanggal }}">
    </div>
    <div class="form-group">
      <label>Status :</label>
      {{ $transactions->status }}
      <input type="hidden" name="status" value="{{ $transactions->status }}">
    </div>
    {{ csrf_field()}}
    <button type="submit" class="btn btn-primary">Confirm</button>
    <a href="{{ url('supplier/transaction')}}" class="btn btn-button">Cancel</a>
  </form> 
</div>
</div>

@endif

@endsection