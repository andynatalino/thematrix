@extends('supplier.layouts.app')

@section('title','Transaction - Supplier')

@section('content')


@if(auth()->guard('supplier')->user()->approved == 1) 
Account supplier masih belum dapat digunakan, untuk mengaktifkannya 
Anda harus membayar sejumlah uang yang ditentukan oleh The Matrix Mall.

Hubungi Call Center kami di 911

<h1>Kode Username Anda</h1><br>
<h2>{{ auth()->guard('supplier')->user()->username }}</h2>

masukan username dan password anda ketika login

@elseif(auth()->guard('supplier')->user()->approved == 2) 

<h2>Transaction Success</h2>  
<table class="table table-striped">
  <thead>
    <tr>
      <th>Transaction Code</th>
      <th>Customer</th>
      <th>Supplier</th>
      <th>Product</th>
      <th>Amount</th>
      <th>Information</th>
      <th>Date</th>
      <th>Status</th>
      <th>Opsi</th>
    </tr>
  </thead>
  <tbody>      
   @foreach($transactions as $key)
   <td>{{ $key->id }}</td>
   <td>{{ $key->customer->name }}</td>   
   <td>{{ $key->supplier->nama }}</td>
   <td>{{ $key->product->nama }}</td>
   <td>{{ $key->jumlah }}</td>
   <td>{{ substr($key->keterangan, 0, 10) }}</td>
   <td>{{ $key->tanggal }} </td>
   <td> @if($key->status == 1) <span class="badge badge-warning">Waiting</span> @elseif($key->status == 2) <span class="badge badge-success">Success</span> @endif </td>
   <td>
    <form action="{{ url('supplier/transaction-success/'.$key->id)}}" method="post">
      <a href="{{ url('supplier/transaction-success/'.$key->id.'/edit')}}" class="btn btn-primary btn-sm">Edit</a>
      <button type="submit" onclick="return confirm('Are you sure to delete?')" class="btn btn-danger btn-sm">Delete</button>
      <input type="hidden" name="_method" value="DELETE">
      {{ csrf_field() }}
    </form>
  </td>
</tr>  
@endforeach
</tbody>
</table>
{{ $transactions->links() }}
@endif

@endsection