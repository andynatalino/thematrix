@extends('supplier.layouts.app')

@section('title','Edit Product - Supplier')

@section('content')


@if(auth()->guard('supplier')->user()->approved == 1) 
Account supplier masih belum dapat digunakan, untuk mengaktifkannya 
Anda harus membayar sejumlah uang yang ditentukan oleh The Matrix Mall.

Hubungi Call Center kami di 911

<h1>Kode Username Anda</h1><br>
<h2>{{ auth()->guard('supplier')->user()->username }}</h2>

masukan username dan password anda ketika login

@elseif(auth()->guard('supplier')->user()->approved == 2) 

<h2>Edit Product</h2>
<form method="post" action="{{ url('supplier/product') }}">
  <div class="form-group">
    <label for="productname">Product Name</label>
    <input type="text" class="form-control" id="productname" name="productname" placeholder="Enter Product Name" required value="{{ $products->nama }}">
  </div>
  <div class="form-group">
    <label for="category">Category</label>
    <input type="text" class="form-control" id="category" name="category" placeholder="Enter Category" required value="{{ $products->jenis }}">
  </div>
  <div class="form-group">
    <label for="price">Price (Rupiah)</label>
    <input type="text" class="form-control" id="price" name="price" placeholder="Enter Price" onkeydown="return numbersonly(this, event);" onkeyup="javascript:tandaPemisahTitik(this);" required value="{{ $products->harga }}">
  </div>
  <div class="form-group">
    <label for="stok">Stok</label>
    <input type="number" class="form-control" id="stok" name="stok" placeholder="Enter Stok" required value="{{ $products->stok }}">
  </div>
  <div class="form-group">
    <label for="status">Status</label>
    <select class="form-control" id="status" name="status" required>
      <option value="1" {{ ($products->status =='1')?'selected':'' }}>Available</option>      
      <option value="2" {{ ($products->status =='2')?'selected':'' }}>Not Available</option>     
    </select>
  </div>
  {{ csrf_field()}}
  <button type="submit" class="btn btn-primary">Save</button>
  <input type="hidden" name="id" value="{{ $products->id }}">
    <a href="{{ url('supplier/product')}}" class="btn btn-button">Cancel</a>
</form> 

<script type="text/javascript">
  function tandaPemisahTitik(b){
    var _minus = false;
    if (b<0) _minus = true;
    b = b.toString();
    b=b.replace(".","");
    b=b.replace("-","");
    c = "";
    panjang = b.length;
    j = 0;
    for (i = panjang; i > 0; i--){
      j = j + 1;
      if (((j % 3) == 1) && (j != 1)){
        c = b.substr(i-1,1) + "." + c;
      } else {
        c = b.substr(i-1,1) + c;
      }
    }
    if (_minus) c = "-" + c ;
    return c;
  }

  function numbersonly(ini, e){
    if (e.keyCode>=49){
      if(e.keyCode<=57){
        a = ini.value.toString().replace(".","");
        b = a.replace(/[^\d]/g,"");
        b = (b=="0")?String.fromCharCode(e.keyCode):b + String.fromCharCode(e.keyCode);
        ini.value = tandaPemisahTitik(b);
        return false;
      }
      else if(e.keyCode<=105){
        if(e.keyCode>=96){
//e.keycode = e.keycode - 47;
a = ini.value.toString().replace(".","");
b = a.replace(/[^\d]/g,"");
b = (b=="0")?String.fromCharCode(e.keyCode-48):b + String.fromCharCode(e.keyCode-48);
ini.value = tandaPemisahTitik(b);
//alert(e.keycode);
return false;
}
else {return false;}
}
else {
  return false; }
}else if (e.keyCode==48){
  a = ini.value.replace(".","") + String.fromCharCode(e.keyCode);
  b = a.replace(/[^\d]/g,"");
  if (parseFloat(b)!=0){
    ini.value = tandaPemisahTitik(b);
    return false;
  } else {
    return false;
  }
}else if (e.keyCode==95){
  a = ini.value.replace(".","") + String.fromCharCode(e.keyCode-48);
  b = a.replace(/[^\d]/g,"");
  if (parseFloat(b)!=0){
    ini.value = tandaPemisahTitik(b);
    return false;
  } else {
    return false;
  }
}else if (e.keyCode==8 || e.keycode==46){
  a = ini.value.replace(".","");
  b = a.replace(/[^\d]/g,"");
  b = b.substr(0,b.length -1);
  if (tandaPemisahTitik(b)!=""){
    ini.value = tandaPemisahTitik(b);
  } else {
    ini.value = "";
  }

  return false;
} else if (e.keyCode==9){
  return true;
} else if (e.keyCode==17){
  return true;
} else {
//alert (e.keyCode);
return false;
}

}
</script> 

@endif

@endsection