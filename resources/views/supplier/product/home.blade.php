@extends('supplier.layouts.app')

@section('title','Product - Supplier')

@section('content')


@if(auth()->guard('supplier')->user()->approved == 1) 
Account supplier masih belum dapat digunakan, untuk mengaktifkannya 
Anda harus membayar sejumlah uang yang ditentukan oleh The Matrix Mall.

Hubungi Call Center kami di 911

<h1>Kode Username Anda</h1><br>
<h2>{{ auth()->guard('supplier')->user()->username }}</h2>

masukan username dan password anda ketika login

@elseif(auth()->guard('supplier')->user()->approved == 2) 

<h2>Manage Product</h2>
<p><a href="{{ url('supplier/product/add')}}"><button type="button" class="btn btn-primary">Add Product</button></a></p>    
<table class="table table-striped">
  <thead>
    <tr>
      <th>Product Name</th>
      <th>Category</th>
      <th>Price</th>
      <th>Stok</th>
      <th>Status</th>
      <th>Opsi</th>
    </tr>
  </thead>
  <tbody>      
   @foreach($products as $key)
   <td>{{ substr($key->nama, 0, 30) }}</td>
   <td>{{ substr($key->jenis, 0, 30) }}</td>
   <td>{{ substr($key->harga, 0, 30) }}</td>
   <td>{{ $key->stok }}</td>
   <td>@if($key->status == 1) <span class="badge badge-success">Available</span> @elseif($key->status == 2) <span class="badge badge-danger">Not Available</span> @endif </td>
   <td>
     <form action="{{ url('supplier/product/'.$key->id)}}" method="post">
      <a href="{{ url('supplier/product/'.$key->id.'/edit')}}" class="btn btn-warning btn-sm">Edit</a>
      <button type="submit" onclick="return confirm('Are you sure to delete?')" class="btn btn-danger btn-sm">Delete</button>
      <input type="hidden" name="_method" value="DELETE">
      {{ csrf_field() }}
    </form>
  </td>
</tr>  
@endforeach
</tbody>
</table>
{{ $products->links() }}
@endif

@endsection