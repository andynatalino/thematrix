@extends('admin.layouts.app')

@section('title','Add Store Category - Admin')

@section('content')
<h2>Add Store Category</h2>
<form method="post" action="{{route('admin.storeCategorySave.submit')}}">
  <div class="form-group">
    <label for="storename">Store Name</label>
    <input type="text" class="form-control" id="storename" name="storename" placeholder="Enter Store Name" required>
  </div>
  {{ csrf_field()}}
  <button type="submit" class="btn btn-primary">Save</button>
  <a href="{{ url('admin/store-category')}}" class="btn btn-button">Cancel</a>
</form> 
@endsection