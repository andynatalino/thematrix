@extends('admin.layouts.app')

@section('title','Edit Store Category - Admin')

@section('content')
<h2>Edit Store Category</h2>
<form method="post" action="{{ url('admin/store-category') }}">
  <div class="form-group">
    <label for="storename">Store Name</label>
    <input type="text" class="form-control" id="storename" name="storename" placeholder="Enter Store Name" required value="{{ $store->name }}">
  </div>
  {{ csrf_field()}}
  <button type="submit" class="btn btn-primary">Save</button>
  <input type="hidden" name="id" value="{{ $store->id }}">
  <a href="{{ url('admin/store-category')}}" class="btn btn-button">Cancel</a>
</form> 
@endsection