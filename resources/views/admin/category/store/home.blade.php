@extends('admin.layouts.app')

@section('title','Store - Supplier')

@section('content')
<h2>Store Category</h2>
<p><a href="{{ url('admin/store-category/add')}}"><button type="button" class="btn btn-primary">Add Store Category</button></a></p>    
<table class="table table-striped">
  <thead>
    <tr>
      <th>Category Code</th>
      <th>Category Name</th>
      <th>Opsi</th>
    </tr>
  </thead>
  <tbody>      
   @foreach($store as $key)
   <td>{{ $key->id }}</td>
   <td>{{ substr($key->name, 0, 30) }}</td>
   <td>
    <form action="{{ url('admin/store-category/'.$key->id)}}" method="post">
      <a href="{{ url('admin/store-category/'.$key->id.'/edit')}}" class="btn btn-primary btn-sm">Edit</a>
      <button type="submit" onclick="return confirm('Are you sure to delete?')" class="btn btn-danger btn-sm">Delete</button>
      <input type="hidden" name="_method" value="DELETE">
      {{ csrf_field() }}
    </form>
  </td>
</tr>  
@endforeach
</tbody>
</table>
{{ $store->links() }}
@endsection