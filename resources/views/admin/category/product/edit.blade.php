@extends('admin.layouts.app')

@section('title','Edit Product Category - Admin')

@section('content')
<h2>Edit Product Category</h2>
<form method="post" action="{{ url('admin/product-category') }}">
	<div class="form-group">
		<label for="productname">product Name</label>
		<input type="text" class="form-control" id="productname" name="productname" placeholder="Enter Product Name" required value="{{ $product->name }}">
	</div>
	<div class="form-group">
		<label for="storename">Store Category</label>
		<select class="form-control" id="storename" name="storename" required>			
			@foreach($store as $key)		
			<option value="{{ $key->id }}">{{ $key->name }}</option>     			     
			@endforeach
		</select>
	</div>
	{{ csrf_field()}}
	<button type="submit" class="btn btn-primary">Save</button>
	<input type="hidden" name="id" value="{{ $product->id }}">
	<a href="{{ url('admin/product-category')}}" class="btn btn-button">Cancel</a>
</form> 
@endsection