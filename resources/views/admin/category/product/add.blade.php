@extends('admin.layouts.app')

@section('title','Add Product Category - Admin')

@section('content')
<h2>Add Product Category</h2>
<form method="post" action="{{route('admin.productCategorySave.submit')}}">
	<div class="form-group">
		<label for="productname">Product Name</label>
		<input type="text" class="form-control" id="productname" name="productname" placeholder="Enter Product Name" required>
	</div>
	<div class="form-group">
		<label for="storename">Store Category</label>
		<select class="form-control" id="storename" name="storename" required>
			@foreach($store as $key)
			<option value="{{ $key->id }}">{{ $key->name }}</option>     			     
			@endforeach
		</select>
	</div>
	{{ csrf_field()}}
	<button type="submit" class="btn btn-primary">Save</button>
	<a href="{{ url('admin/product-category')}}" class="btn btn-button">Cancel</a>
</form> 
@endsection