@extends('admin.layouts.app')

@section('title','Supplier Confirm')

@section('content')
<div class="jumbotron">
 <h1 class="display-9">Supplier Confirm</h1>
 <div class="container">

  <form method="post" action="{{ route('supplier.supplierConfirmSave.submit') }}">
    <div class="form-group">
      <label>Supplier Code :</label>
      {{ $supplier->id }}
      <input type="hidden" name="id" value="{{ $supplier->id }}">
    </div>
    <div class="form-group">
      <label>Store Name :</label>
      {{ $supplier->nama }}
      <input type="hidden" name="storename" value=" {{ $supplier->nama }}">
    </div>
    <div class="form-group">
      <label>Address :</label>
      {{ $supplier->alamat }}
      <input type="hidden" name="address" value=" {{ $supplier->alamat }}">
    </div>
    <div class="form-group">
      <label>Email :</label>
      {{ $supplier->email }}
      <input type="hidden" name="email" value="{{ $supplier->email }}">
    </div>
    <div class="form-group">
      <label>Category Store :</label>
      {{ $supplier->jenistoko }}
      <input type="hidden" name="Category" value=" {{ $supplier->jenistoko }}">
    </div>
    <div class="form-group">
      <label>Slogan :</label>
      {{ $supplier->slogan }}
      <input type="hidden" name="slogan" value="{{ $supplier->slogan }}">
    </div>
    <div class="form-group">
      <label>Username :</label>
      {{ $supplier->username }}
      <input type="hidden" name="date" value=" {{ $supplier->username }}">
    </div>
    <div class="form-group">
      <label>Status :</label>
      @if($supplier->approved == 1) <span class="badge badge-danger">Not Active</span> @elseif($supplier->approved == 2) <span class="badge badge-success">Active</span> @endif
      <input type="hidden" name="status" value="{{ $supplier->approved }}">
    </div>
    {{ csrf_field()}}
    <button type="submit" class="btn btn-primary">Confirm</button>
    <a href="{{ url('admin')}}" class="btn btn-button">Cancel</a>
  </form> 
</div>
</div>

@endsection