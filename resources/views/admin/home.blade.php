@extends('admin.layouts.app')

@section('title','Dashboard - Admin')

@section('content')

<h2>Supplier Not Active</h2>
<table class="table table-striped">
	<thead>
		<tr>
			<th>Name</th>
			<th>Address</th>
			<th>Category</th>
			<th>Slogan</th>
			<th>Username</th>
			<th>Status</th>
			<th>Opsi</th>
		</tr>
	</thead>
	<tbody>      
		@foreach($supplier as $key)
		<td>{{ substr($key->nama, 0, 30) }}</td>
		<td>{{ substr($key->alamat, 0, 30) }}</td>
		<td>{{ substr($key->jenistoko, 0, 30) }}</td>
		<td>{{ substr($key->slogan, 0, 30) }}</td>
		<td>{{ substr($key->username, 0, 30) }}</td>
		<td>@if($key->approved == 1) <span class="badge badge-danger">Not Active</span> @elseif($key->approved == 2) <span class="badge badge-success">Active</span> @endif </td>
		<td>
			<form action="{{ url('admin/supplier/'.$key->id)}}" method="post">
				<a href="{{ url('admin/supplier/'.$key->id.'/confirm')}}" class="btn btn-success btn-sm">Confirm</a>
				<button type="submit" onclick="return confirm('Are you sure to delete?')" class="btn btn-danger btn-sm">Delete</button>
				<input type="hidden" name="_method" value="DELETE">
				{{ csrf_field() }}
			</form>
		</td>
	</tr>  
	@endforeach
</tbody>
</table>
{{ $supplier->links() }}

@endsection