<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class transaksi extends Model
{
      public function customer(){
            return $this->belongsTo('App\User', 'customer_id');
      }

      public function supplier(){
            return $this->belongsTo('App\supplier', 'supplier_id');
      }

      public function product(){
            return $this->belongsTo('App\product', 'product_id');
      }
}