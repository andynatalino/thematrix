<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Notifications\Notifiable;
use Illuminate\Foundation\Auth\User as Authenticatable;

class supplier extends Authenticatable
{
    use Notifiable;

    protected $fillable = [
        'nama', 'alamat', 'email', 'jenistoko', 'slogan', 'username', 'password',
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];

    public function storecategory(){
        return $this->belongsTo('App\storecategory', 'jenistoko');
    }
     
}