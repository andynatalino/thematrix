<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class productcategory extends Model
{
	public function storecategory(){
		return $this->belongsTo('App\storecategory', 'storecategory_id');
	}
}
