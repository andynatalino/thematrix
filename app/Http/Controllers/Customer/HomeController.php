<?php

namespace App\Http\Controllers\Customer;

use Auth;
use Hash;
use App\User;
use App\Product;
use App\Supplier;
use App\Transaksi;
use App\Storecategory;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
      $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
    	return view('customer.home');
    }

    public function shop()
    {
        $store = Storecategory::get();
        $supplier = Supplier::paginate(5);        
        return view('customer.shop', ['supplier' => $supplier, 'store' => $store]);
    }

    public function shopSearch(Request $request){
        $query = $request->q;       
        $supplier = Supplier::where('nama','like','%'.$query.'%')
        ->orWhere('jenistoko', 'like','%'.$query.'%')
        ->orderBy('id','asc')->paginate(5);      
        $store = Storecategory::get();
        return view('customer.search', ['supplier' => $supplier, 'store' => $store]);
    }

    public function shopShow($id)
    {        
       $supplier = Supplier::find($id);       
       $product = Product::where('supplier_id', $supplier->id)->where('status', '1')->get(); 
       return view('customer.show', ['supplier' => $supplier, 'product' => $product]);
    }
    public function shopSearchShow(Request $request){
        $query = $request->q;               
        $supplier = Supplier::find($request->id);   
        $product = Product::where('nama','like','%'.$query.'%')       
        ->orderBy('id','asc')
        ->where('status', '1')
        ->paginate(5);      
        return view('customer.showsearch', ['supplier' => $supplier, 'product' => $product]);
    }

    public function shopProduct($id)
    {                          
       $product = Product::where('id', $id)->first();
       $supplier = Supplier::find($product->supplier_id);
       return view('customer.product', ['supplier' => $supplier, 'product' => $product]);
    }

    public function shopBuy(Request $request)
    {                
        $transaction = new Transaksi;
        $transaction->customer_id = Auth::user()->id;
        $transaction->supplier_id = $request->supplier_id;
        $transaction->product_id = $request->product_id;
        $transaction->jumlah = $request->amount;
        $transaction->keterangan = $request->information;        
        $transaction->tanggal = date("Y/m/d:h:i:s");
        $transaction->save();

        return redirect(url('shop'));
    }

    public function transaction()
    {
        $transactions = Transaksi::paginate(5);        
        return view('customer.transaction.home', ['transactions' => $transactions]);
    }

    public function settings()
    {
        $id = Auth::user()->id;
        $user = User::find($id);

        return view('customer.settings.home', ['user' => $user]);
    }

    public function settingsSave(Request $request)
    {
        $id = Auth::user()->id;
        $user = User::find($id);    
        $user->name = $request->nama;
        $user->username = $request->username;
        $user->email = $request->email;
        $user->save();

        return redirect(url('home'));
    }

    public function changepassword()
    {
      $id = Auth::user()->id;
      $user = User::find($id);
     return view('customer.settings.changepassword.home', ['user' => $user]);
    }

    public function changepasswordSave(Request $request)
    {
       $a = User::find(Auth::user()->id);
       if (Hash::check($request->oldpw, $a['password']) && $request->newpw == $request->confnewpw) {
       
        $a->password = bcrypt($request->newpw);
        $a->save();
        
        return redirect(url('settings'));
      }else {
        
          return back()->with('gagal', 'Password yang anda masukan tidak sesuai!');
      }
    }
}
