<?php

namespace App\Http\Controllers\Supplier;

use Auth;
use Hash;
use App\Product;
use App\Supplier;
use App\Transaksi;
use App\Productcategory;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth:supplier');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {            
        return view('supplier.home');
    }

    public function product()
    {       
       $products = Product::paginate(5);
        return view('supplier.product.home', ['products' => $products]);
    }

    public function productAdd()
    {
      $id = auth::guard('supplier')->user()->id;
      $supplier = Supplier::find($id);
      $product = Productcategory::where('storecategory_id', $supplier->jenistoko)->get();  
      // dd($product);  
       return view('supplier.product.add', ['product' => $product]);
    }

    public function productSave(Request $request)
    {
       Product::create([
        'supplier_id'   => auth::guard('supplier')->user()->id,
        'nama'  => $request->productname,
        'harga'      => $request->price,
        'jenis'  => $request->category,
        'stok'  => $request->stok,
        'status' => $request->status,    
      ]);

      return redirect(url('supplier/product'));
    }
    public function productEdit($id)
    {
      $products = Product::find($id);      
      return view('supplier.product.edit', ['products' => $products]);
    }

    public function productUpdate(Request $request)
    {
       $products = Product::find($request->id);
       $products->supplier_id = auth::guard('supplier')->user()->id;
       $products->nama = $request->productname;         
       $products->harga = $request->price;
       $products->jenis = $request->category;
       $products->stok = $request->stok;
       $products->status = $request->status;
       $products->save();
      
      return redirect(url('supplier/product'));
    }

    public function productDelete($id)
    {
        $product = Product::find($id);
        $product->delete();

        return redirect(url('supplier/product'));
    }

    public function transaction()
    {
      $transactions = Transaksi::where('status', 1)->paginate(5);
      return view('supplier.transaction.home', ['transactions' => $transactions]);
    }

    public function transactionConfirm($id)
    {
      $transactions = Transaksi::find($id);      
      return view('supplier.transaction.confirm', ['transactions' => $transactions]);
    }

    public function transactionConfirmSave(Request $request)
    {      
       $transaction = Transaksi::findOrFail($request->id);
       $products = Product::find($transaction->product_id);
       $transaction->customer_id = $request->customer;
       $transaction->supplier_id = $request->supplier;
       $transaction->product_id = $request->product;
       $hasil = $products->stok - $request->amount;       
       $products->stok = $hasil;
       $products->save();
       $transaction->keterangan = $request->information;
       $transaction->tanggal = $request->date;
       $transaction->status = 2;
       $transaction->save();
      
      return redirect(url('supplier/transaction'));
    }

    public function transactionsuccess()
    {
     $transactions = Transaksi::where('status', 2)->paginate(5);
      return view('supplier.transactionsuccess.home', ['transactions' => $transactions]);
    }
    
    public function transactionsuccessEdit($id)
    {
      $transactions = Transaksi::find($id);      
      return view('supplier.transactionsuccess.edit', ['transactions' => $transactions]);
    }

    public function transactionsuccessUpdate(Request $request)
    {
       $transaction = Transaksi::findOrFail($request->id);
       $products = Product::find($transaction->product_id);
       $transaction->customer_id = $request->customer;
       $transaction->supplier_id = $request->supplier;
       $transaction->product_id = $request->product;
       $hasil = $products->stok + $request->amount;       
       $products->stok = $hasil;
       $products->save();
       $transaction->keterangan = $request->information;
       $transaction->tanggal = $request->date;
       $transaction->status = $request->status;
       $transaction->save();

        return redirect(url('supplier/transaction-success'));
    }

    public function transactionsuccessDelete($id)
    {
        $transaction = Transaksi::find($id);
        $transaction->delete();

        return redirect(url('supplier/transaction-success'));
    }

    public function settings()
    {
     $id = Auth::guard('supplier')->user()->id;
     $supplier = Supplier::find($id);

     return view('supplier.settings.home', ['supplier' => $supplier]);
    }

    public function settingsSave(Request $request)
    {
     $supplier = Supplier::find($request->id);     
     $supplier->nama = $request->storename;
     $supplier->alamat = $request->address;
     $supplier->email = $request->email;     
     $supplier->slogan = $request->slogan;
     $supplier->approved = 2;
     $supplier->save();

      return redirect(url('supplier/settings'));
    }

    public function settingsChangePassword(Request $request)
    {
      $id = Auth::guard('supplier')->user()->id;
      $supplier = Supplier::find($id);
     return view('supplier.settings.changepassword.home', ['supplier' => $supplier]);
    }

    public function settingsChangePasswordSave(Request $request)
    {            
       $a = Supplier::find(auth::guard('supplier')->user()->id);
      if (Hash::check($request->oldpw, $a['password']) && $request->newpw == $request->confnewpw) {
       
        $a->password = bcrypt($request->newpw);
        $a->save();
        
        return redirect(url('supplier/settings'));
      }else {
        
          return back()->with('gagal', 'Password yang anda masukan tidak sesuai!');
      }
    }
}
