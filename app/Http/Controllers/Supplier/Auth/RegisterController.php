<?php

namespace App\Http\Controllers\Supplier\Auth;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

//Validator facade used in validator method
use Illuminate\Support\Facades\Validator;

//Seller Model
use App\Supplier;

//Auth Facade used in guard
use Auth;

class RegisterController extends Controller
{

     protected $redirectPath = '/supplier/home';

    //shows registration form to seller
    public function showRegistrationForm()
    {
        return view('supplier.auth.register');
    }

  //Handles registration request for seller
    public function register(Request $request)
    {

       //Validates data
        $this->validator($request->all())->validate();

       //Create seller
        $supplier = $this->create($request->all());

        //Authenticates seller
        $this->guard()->login($supplier);

       //Redirects sellers
        return redirect($this->redirectPath);
    }

    //Validates user's Input
    protected function validator(array $data)
    {
        return Validator::make($data, [
            'nama'          => 'required|min:5|max:35',
            'alamat'      => 'required',
            'email'      => 'required|email',
            // 'jenistoko'      => 'required|min:5|max:35',
            'slogan'      => 'required|min:5|max:35',
            // 'username'      => 'required|min:5|max:35',
            'password'      => 'required|string|min:6|confirmed',
        ]);
    }

    //Create a new seller instance after a validation.
    protected function create(array $data)
    {
     return Supplier::create([
        'nama'      => $data['nama'],   
        'alamat'    => $data['alamat'],
        'email'     => $data['email'],
        'jenistoko' => $data['jenistoko'],
        'slogan'    => $data['slogan'],
        'username'  => time()+2*5-10,
        'password'  => bcrypt($data['password']),
        'approved'  => 2,
    ]);        
 }

    //Get the guard to authenticate Seller
 protected function guard()
 {
     return Auth::guard('supplier');
 }

}