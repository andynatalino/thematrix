<?php

namespace App\Http\Controllers\Admin;

use Auth;
use App\Supplier;
use App\Productcategory;
use App\Storecategory;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth:admin');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {         
        $supplier = Supplier::where('approved', '1')->paginate(5); 
        return view('admin.home', ['supplier' => $supplier]);
    }

    public function supplierConfirm($id)
    {
       $supplier = Supplier::find($id);      
       return view('admin.supplier.confirm', ['supplier' => $supplier]);
   }

    public function supplierConfirmSave(Request $request)
    {
       $supplier = Supplier::findOrFail($request->id);      
       $supplier->nama = $request->storename;
       $supplier->alamat = $request->address;
       $supplier->email = $request->email;     
       $supplier->slogan = $request->slogan;
       $supplier->approved = 2;
       $supplier->save();

       return redirect(url('admin'));
   }

    public function supplierDelete($id)
    {
       $supplier = Supplier::find($id);
       $supplier->delete();

       return redirect(url('admin'));
    }

    public function storeCategory()
    {
       $store = Storecategory::paginate(5);      
       return view('admin.category.store.home', ['store' => $store]);
    }

    public function storeCategoryAdd()
    {   
       return view('admin.category.store.add');
    }

    public function storeCategorySave(Request $request)
    {   
      $store = new Storecategory;
      $store->name = $request->storename;
      $store->save();
     
      return redirect(url('admin/store-category'));
    }

    public function storeCategoryEdit($id)
    {   
      $store = Storecategory::findOrFail($id);
      return view('admin.category.store.edit', ['store' => $store]);
    }

    public function storeCategoryUpdate(Request $request)
    {   
      $store = Storecategory::find($request->id);
      $store->name = $request->storename;
      $store->save();
     
      return redirect(url('admin/store-category'));
    }

    public function storeCategoryDelete(Request $request)
    {
      $store = Storecategory::find($request->id);
      $store->delete();
     
      return redirect(url('admin/store-category'));
    }

    public function productCategory()
    {
       $product = Productcategory::paginate(5);      
       return view('admin.category.product.home', ['product' => $product]);
    }

    public function productCategoryAdd()
    {   
       $store = Storecategory::get();
       return view('admin.category.product.add', ['store' => $store]);
    }

    public function productCategorySave(Request $request)
    {   
      $product = new Productcategory;
      $product->name = $request->productname;
      $product->storecategory_id = $request->storename;
      $product->save();
     
      return redirect(url('admin/product-category'));
    }

    public function productCategoryEdit($id)
    {   
      $store = Storecategory::get();
      $product = Productcategory::findOrFail($id);
      return view('admin.category.product.edit', ['product' => $product, 'store' => $store]);
    }

    public function productCategoryUpdate(Request $request)
    {   
      $product = Productcategory::find($request->id);
      $product->name = $request->storename;
      $product->storecategory_id = $request->storename;
      $product->save();
     
      return redirect(url('admin/product-category'));
    }

    public function productCategoryDelete(Request $request)
    {
      $product = Productcategory::find($request->id);
      $product->delete();
     
      return redirect(url('admin/product-category'));
    }


}
