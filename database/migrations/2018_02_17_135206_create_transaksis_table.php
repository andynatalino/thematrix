<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTransaksisTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('transaksis', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('customer_id')->unsigned();
            $table->integer('supplier_id')->unsigned(); 
            $table->integer('product_id')->unsigned(); 
            $table->string('jumlah', 100);  
            $table->string('keterangan', 100);  
            $table->tinyInteger('status')->default(1);
            $table->string('tanggal', 100);  
            $table->timestamps();

            $table->foreign('customer_id')->references('id')
            ->on('users')->onDelete('cascade');
            $table->foreign('supplier_id')->references('id')
            ->on('suppliers')->onDelete('cascade');           
            $table->foreign('product_id')->references('id')
            ->on('products')->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('transaksis');
    }
}
