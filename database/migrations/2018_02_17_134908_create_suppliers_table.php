<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateSuppliersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('suppliers', function (Blueprint $table) {
            $table->increments('id');
            $table->string('nama', 50);
            $table->string('alamat', 100);
            $table->string('email', 50)->unique();
            $table->integer('jenistoko')->unsigned();
            $table->string('slogan', 100);
            $table->string('username', 50)->unique()->nullable();
            $table->string('password', 191);
            $table->tinyInteger('approved')->default(1);
            $table->rememberToken();
            $table->timestamps();

            $table->foreign('jenistoko')->references('id')
            ->on('storecategories')->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('suppliers');
    }
}
