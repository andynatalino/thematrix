<?php

use Illuminate\Database\Seeder;

class adminTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
    	DB::table('admins')->insert([            
    		'name' => 'andynatalino',
    		'email' => 'andy@admin.com',
    		'password' => '$2y$10$xAmaUeYNKoyxPfkT5.CYjOIyoNFxIitUSiedJzZrVvSRk/7KgKla.',
            'remember_token' => '',            
    	]);

        DB::table('storecategories')->insert([            
            'name' => 'restaurant',           
        ]);

        DB::table('storecategories')->insert([            
            'name' => 'stationery',           
        ]);

        DB::table('productcategories')->insert([      
           'storecategory_id' => '1',        
           'name' => 'Makanan',           
       ]);

        DB::table('productcategories')->insert([      
           'storecategory_id' => '1',        
           'name' => 'Minuman',           
       ]);

          DB::table('productcategories')->insert([      
           'storecategory_id' => '2',        
           'name' => 'Book',           
       ]);

        DB::table('productcategories')->insert([      
           'storecategory_id' => '2',        
           'name' => 'Bag',           
       ]);

    }
}
